import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output() recipeWasSelected = new EventEmitter<Recipe>();
  recipes: Recipe[] = [
    new Recipe(
      'Test',
      'Test Description',
      'http://images2.fanpop.com/image/photos/8600000/random-animals-animals-8676039-1600-1200.jpg'
    ),
    new Recipe(
      'Test2',
      'Second Test Description',
      'http://images2.fanpop.com/image/photos/8600000/random-animals-animals-8676039-1600-1200.jpg'
    )
  ];

  constructor() {}

  ngOnInit() {}
  onSelection(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }
}
