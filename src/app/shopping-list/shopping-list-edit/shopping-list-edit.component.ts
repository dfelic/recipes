import {
  Component,
  OnInit,
  ViewChild,
  EventEmitter,
  Output,
  ElementRef
} from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit {
  @ViewChild('nameInput', { static: false }) nameInputRef: ElementRef;
  @ViewChild('amountInput', { static: false }) amountInputRef: ElementRef;
  @Output() ingredientAdded = new EventEmitter<Ingredient>();

  constructor() {}

  ngOnInit() {}
  onAddIngredient() {
    const ingredName = this.nameInputRef.nativeElement.value;
    const ingredAmount = this.amountInputRef.nativeElement.value;
    const newIngredient = new Ingredient(ingredName, ingredAmount);
    this.ingredientAdded.emit(newIngredient);
  }
}
